#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#define M 256

struct BOOK{
	char title[M];
	char author[M];
	int year;
};

//���������� ����
int book_coun(){
	int coun = 0;
	int i = 0;
	FILE *file;
	file = fopen("input1.txt", "r");
	
	char buf[M];
	while (fgets(buf, M, file)){
		if (buf[0] == '\n'){
			coun++;
		}
	}
	
	return ++coun;
}


//������ ����� � ������ ��������
void read_books( struct BOOK *Books, int num){
	FILE *file;
	file = fopen("input1.txt","r");
	char buf[M];
	for (int i = 0; i < num; i++){
		fgets(Books[i].title, M, file);
		Books[i].title[strlen(Books[i].title) - 1] = '\0';
		
		fgets(Books[i].author, M, file);
		Books[i].author[strlen(Books[i].author) - 1] = '\0';

		fgets(buf, M, file);
		Books[i].year = atoi(buf);

		fgets(buf, M, file);
	}
	fclose(file);
}

//Compare books
int BookComp(BOOK book1, BOOK book2){
	/*0 - ������ �� ����
	  1 - ������ ���� */
	int len,flag=2;
	//������ ����� ���������
	if (strlen(book1.author) == strlen(book2.author)){
		len = strlen(book1.author);
	}
	else if (strlen(book1.author) > strlen(book2.author)){
		len = strlen(book2.author);
	}
	else if (strlen(book1.author) > strlen(book2.author)){
		len = strlen(book1.author);
	}
	
	//���� ���������

	for (int i = 0; i < len; i++){
		if (book1.author[i] > book2.author[i]){
			
			flag = 1;
			break;
		}
		else if (book1.author[i] < book2.author[i]){
			flag = 0;
			break;
		}
	}
	return flag;
}
int main(){
	int a = book_coun();
	struct BOOK *books;
	books = (struct BOOK*)malloc(a*sizeof(struct BOOK));
	read_books(books, a);
	//������� #1
	printf("%i\n\n\n",a);
	printf("\ntask 5.1\n 1)\n");
	for (int i = 0; i <= 3; i++){
		printf("%i: %s  %s  %i\n", i+1, books[i].title, books[i].author, books[i].year);
	}
	
	//������� #2
	printf("\ntask 5.1\n 2)\n");
	int min = 3000, mincoun = 0, max = 0, maxcoun = 0;
	for (int i = 0; i <= 3; i++){
		if (books[i].year > max){
			max = books[i].year;
			maxcoun = i;
		}
		if (books[i].year < min){
			min = books[i].year;
			mincoun = i;
		}
	}
	printf("Oldest book\n");
	printf("%i: %s  %s  %i\n", maxcoun+1, books[maxcoun].title, books[maxcoun].author, books[maxcoun].year);
	printf("Youngest book\n");
	printf("%i: %s  %s  %i\n", mincoun+1, books[mincoun].title, books[mincoun].author, books[mincoun].year);

	printf("\ntask 5.1\n 3)\n");
	
	
	//������� #3
	int *arrbooks;
	
	//������� ������ � �������� ����
	arrbooks = new int[a];
	for (int p = 0; p < a; p++){
		arrbooks[p] = p;
	}
	int temp = 0;
	for (int i = a - 1; i >= 0; i--){
		for (int j = 0; j < i; j++){
			if (BookComp(books[arrbooks[j]], books[arrbooks[j+1]]) == 1){
				temp = arrbooks[j];
				arrbooks[j] = arrbooks[j+1];
				arrbooks[j+1]=temp;
			}
		}
	}
	for (int i = 0; i <= 3; i++){
		printf("%i: %s  %s  %i\n", i + 1, books[arrbooks[i]].title, books[arrbooks[i]].author, books[arrbooks[i]].year);
	}
	return 0;
}

